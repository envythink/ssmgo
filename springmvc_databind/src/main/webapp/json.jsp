<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2020/3/20
  Time: 22:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>json类型数据绑定</title>
</head>
<body>
<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
    $(function () {
        var course = {
            id: "2",
            name: "SpringMVC数据绑定",
            price: "188"
        };
        $.ajax({
            url:"jsonType",
            data:JSON.stringify(course),
            type:"post",
            contentType: "application/json;charset=UTF-8",
            dataType:"json",
            success:function (data) {
                alert(data.name+"-----"+data.price)
            }
        })
    })
</script>
</body>
</html>
