package com.envy.dao;

import com.envy.entity.Course;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class CourseDao {
    //使用Map来替代数据库
    private Map<Integer, Course> courses = new HashMap<>();

    //将pojoType方法中传递的model保存到map中
    public void add(Course course){
        courses.put(course.getId(), course);
    }

    //将map中所有的信息进行返回
    public Collection<Course> getAll(){
        return courses.values();
    }
}
