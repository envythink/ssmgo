package com.envy.entity;

import java.util.List;

public class CourseList {
    private List<Course> courses;  //属性名称

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
