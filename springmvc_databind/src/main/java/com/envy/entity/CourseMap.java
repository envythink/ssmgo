package com.envy.entity;

import java.util.Map;

public class CourseMap {
    private Map<String,Course> courseMap; //属性名称

    public Map<String, Course> getCourseMap() {
        return courseMap;
    }

    public void setCourseMap(Map<String, Course> courseMap) {
        this.courseMap = courseMap;
    }
}
