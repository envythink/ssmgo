package com.envy.entity;

import java.util.HashSet;
import java.util.Set;

public class CourseSet {
    private Set<Course> courseSet = new HashSet<>(); //属性名称

    public Set<Course> getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(Set<Course> courseSet) {
        this.courseSet = courseSet;
    }

    //注意set中必须有两个对象，否则无法完成绑定
    public CourseSet(){
        courseSet.add(new Course());
        courseSet.add(new Course());
    }
}
