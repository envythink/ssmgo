package com.envy.controller;

import com.envy.dao.CourseDao;
import com.envy.entity.Course;
import com.envy.entity.CourseList;
import com.envy.entity.CourseMap;
import com.envy.entity.CourseSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class DataBindController {

    @Autowired
    private CourseDao courseDao;

    //基本数据类型，以int为例
    @RequestMapping(value = "/baseType")
    @ResponseBody  //@ResponseBody这个注解就是将返回的对象转换为json格式的字符串，并通过response响应给客户端
    public String baseType(@RequestParam(value = "id")int id){
        return "this baseType id is "+id;
    }

    //包装类,以Integer为例
    @RequestMapping(value = "/packageType")
    @ResponseBody
    public String packageType(@RequestParam(value = "id")Integer id){
        return "this packageType id is "+id;
    }

    //数组类型，以String类型的数组为例
    @RequestMapping(value = "/arrayType")
    @ResponseBody
    public String arrayType(String[] name){  //此处的name即为参数的名称
        StringBuffer stringBuffer = new StringBuffer();
        for(String item:name) {
            stringBuffer.append(item).append(" ");
        }
        return stringBuffer.toString();
    }


    //对象,以课程和讲师为例
    @RequestMapping(value = "/pojoType")
    public ModelAndView pojoType(Course course){
        //保存前端页面提交信息
        courseDao.add(course);
        //装载模型数据和视图解析
        ModelAndView modelAndView = new ModelAndView();
        //添加模型数据
        modelAndView.addObject("courses", courseDao.getAll());
        //设置视图名称
        modelAndView.setViewName("index");
        return modelAndView;
    }


    //List，注意这里面不能直接传参数List<Course> courses，因为它无法完成绑定（只能绑定单一类）
    //只能借助于包装类，在包装类中定义List集合的属性，然后给这个包装类的属性进行绑定
    @RequestMapping(value = "/listType")
    public ModelAndView listType(CourseList courseList){
        //注意这里遍历的是courseList中的属性即List对象，不是courseList本身
        for(Course course:courseList.getCourses()){
            //保存前端页面提交信息
            courseDao.add(course);
        }
        //装载模型数据和视图解析
        ModelAndView modelAndView = new ModelAndView();
        //添加模型数据
        modelAndView.addObject("courses", courseDao.getAll());
        //设置视图名称
        modelAndView.setViewName("index");
        return modelAndView;
    }

    //Map，注意这里面不能直接传参数Map<String,Course> courseMap，因为它无法完成绑定（只能绑定单一类）
    //只能借助于包装类，在包装类中定义Map的属性，然后给这个包装类的属性进行绑定
    @RequestMapping(value = "/mapType")
    public ModelAndView mapType(CourseMap courseMap){
        //注意这里遍历的是courseMap中的属性即Map对象，不是courseMap本身
        for(String key:courseMap.getCourseMap().keySet()){
            Course course = courseMap.getCourseMap().get(key);
            courseDao.add(course);
        }
        //装载模型数据和视图解析
        ModelAndView modelAndView = new ModelAndView();
        //添加模型数据
        modelAndView.addObject("courses", courseDao.getAll());
        //设置视图名称
        modelAndView.setViewName("index");
        return modelAndView;
    }

    //Set，注意这里面不能直接传参数Set<Course> courseSet，因为它无法完成绑定（只能绑定单一类）
    //只能借助于包装类，在包装类中定义Map的属性，然后给这个包装类的属性进行绑定
    @RequestMapping(value = "/setType")
    public ModelAndView setType(CourseSet courseSet){
        //注意这里遍历的是courseSet中的属性即Set对象，不是courseSet本身
        for(Course course:courseSet.getCourseSet()){
            courseDao.add(course);
        }
        //装载模型数据和视图解析
        ModelAndView modelAndView = new ModelAndView();
        //添加模型数据
        modelAndView.addObject("courses", courseDao.getAll());
        //设置视图名称
        modelAndView.setViewName("index");
        return modelAndView;
    }

    //JSON，注意此时不需要通过视图解析，直接返回JSOn格式
    //需要使用@RequestBody这个注解,这个注解主要用来接收前端传递给后端的json字符串中的数据(请求体中的数据);
    @RequestMapping(value = "/jsonType")
    @ResponseBody
    public Course jsonType(@RequestBody Course course){
        course.setPrice(course.getPrice()+100);
        return course;
    }
}

