package com.envy.controller;

import com.envy.dao.CourseDao;
import com.envy.entity.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CourseController {

    @Autowired
    private CourseDao courseDao;

    //添加课程
    @PostMapping(value = "/add")
    public String add(Course course){
        courseDao.add(course);
        return "redirect:/getAll";
    }

    //查询全部课程
    @GetMapping(value = "/getAll")
    public ModelAndView getAll(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("courses",courseDao.getALl());
        modelAndView.setViewName("index");
        return modelAndView;
    }

    //通过id来查询某个课程
    @GetMapping(value = "/get/{id}")
    public ModelAndView get(@PathVariable(value = "id")int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("course",courseDao.getOne(id));
        modelAndView.setViewName("edit");
        return modelAndView;
    }

    //修改课程
    @PutMapping(value = "/update")
    public String update(Course course){
        courseDao.update(course);
        return "redirect:/getAll";
    }

    //删除某个课程
    @DeleteMapping(value = "/delete/{id}")
    public String delete(@PathVariable(value = "id")int id){
        courseDao.delete(id);
        return "redirect:/getAll";
    }
}
