package com.envy.handler;

import com.envy.entity.Goods;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class MyNewHandler{
    //自己定义方法用于返回装载模型数据和逻辑视图的ModelAndView
    @RequestMapping("/getModelAndView")
    public ModelAndView getModelAndView(){
        //装载模型数据和逻辑视图
        ModelAndView modelAndView = new ModelAndView();
        //添加模型数据
        modelAndView.addObject("sex","male");
        //设置逻辑视图
        modelAndView.setViewName("good");
        return modelAndView;
    }


    //业务方法：model传值、String用于视图解析
    @RequestMapping("/ModelStringTest")
    public String ModelStringTest(Model model){
        //添加模型数据
        model.addAttribute("sex","male");
        //设置逻辑视图
        return "good";
    }

    //业务方法：map传值、String用于视图解析
    @RequestMapping("/MapTest")
    public String MapTest(Map<String,String> map){
        //添加模型数据
        map.put("sex","male");
        //设置逻辑视图
        return "good";
    }

    //添加商品并展示
    @RequestMapping("/addGoods")
    public ModelAndView addGoods(Goods goods){
        System.out.println(goods.getName()+"---"+goods.getPrice());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("goods",goods);
        modelAndView.setViewName("goods");
        return modelAndView;
    }

}
