package com.envy.dao;

import com.envy.entity.Selection;
import com.envy.entity.Student;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SelectionDao {
    public void insert(List<Selection> selectionList);
    public void delete(int studentId,int courseId);
    public List<Map<String,Object>> selectByStudentId(int studentId);
    public List<Map<String,Object>> selectByCourseId(int courseId);
}
