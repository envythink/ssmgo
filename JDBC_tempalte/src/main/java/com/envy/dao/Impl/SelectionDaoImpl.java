package com.envy.dao.Impl;

import com.envy.dao.SelectionDao;
import com.envy.entity.Selection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class SelectionDaoImpl implements SelectionDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void insert(List<Selection> selectionList) {
        String sql = "insert into selection(studentId,courseId,score,selectionTime)values(?,?,?,?)";
        List<Object[]> list = new ArrayList<Object[]>();
        for(Selection selection:selectionList){
            Object[] args = new Object[4];
            args[0] = selection.getStudentId();
            args[1] = selection.getCourseId();
            args[2] = selection.getScore();
            args[3] = selection.getSelectionTime();
            list.add(args);
        }
        jdbcTemplate.batchUpdate(sql,list);
    }

    public void delete(int studentId,int courseId) {
        String sql = "delete * from selection where studentId=? and courseId =?";
        jdbcTemplate.update(sql,studentId,courseId);
    }

    public List<Map<String, Object>> selectByStudentId(int studentId) {
        String sql = "select se.*,stu.name sname,cou.name cname from selection se"+
                "left join student stu on se.studentId = stu.id"+
                "left join course cou on se.courseId = cou.id"+
                "where studentId=?";
        return jdbcTemplate.queryForList(sql,studentId);
    }

    public List<Map<String, Object>> selectByCourseId(int courseId) {
        String sql = "select se.*,stu.name sname,cou.name cname from selection se"+
                "left join student stu on se.studentId = stu.id"+
                "left join course cou on se.courseId = cou.id"+
                "where courseId=?";
        return jdbcTemplate.queryForList(sql,courseId);
    }
}
