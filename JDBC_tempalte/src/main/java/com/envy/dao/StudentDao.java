package com.envy.dao;

import com.envy.entity.Student;

import java.util.List;

public interface StudentDao {
    public void insert(Student student);
    public void delete(int id);
    public void update(Student student);
    public Student find(int id);
    public List<Student> findAll();
}
