package com.envy.dao;

import com.envy.entity.Course;

import java.util.List;

public interface CourseDao {
    public void insert(Course course);
    public void delete(int id);
    public void update(Course course);
    public Course find(int id);
    public List<Course> findAll();
}
