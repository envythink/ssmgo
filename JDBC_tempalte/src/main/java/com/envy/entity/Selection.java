package com.envy.entity;

import java.util.Date;

public class Selection {
    private int studentId;
    private int courseId;
    private Date selectionTime;
    private int score;

    public int getStudentId(){
        return studentId;
    }

    public void setStudentId(int studnetId){
        this.studentId =studnetId;
    }

    public int getCourseId(){
        return courseId;
    }

    public void setCourseId(int courseId){
        this.courseId=courseId;
    }

    public Date getSelectionTime(){
        return selectionTime;
    }

    public void setSelectionTime(Date selectionTime){
        this.selectionTime=selectionTime;
    }

    public int getScore(){
        return score;
    }

    public void setScore(int score){
        this.score=score;
    }
}
