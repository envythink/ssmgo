create database mybatis_parameter;
drop table if exists person;
create table person(
id int PRIMARY key auto_increment,
username varchar(20),
email varchar(20),
gender varchar(20),
department_id int
);

drop table if exists department;
create table department(
id int primary key auto_increment,
department_name varchar(50)
);

alter table person add constraint fk_person_department foreign key(department_id) references department(id);