package com.envy.dao;

import com.envy.bean.Person;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface PersonMapper {
    void deletePerson(Integer id);

    //封装成POJO对象
//    Person selectByUserNameAndGender(Person person);

    //封装成Map
//    Person selectByUserNameAndGender(Map<String,Object> param);

    //使用Param注解
    Person selectByUserNameAndGender(@Param("UserName")String username,@Param("Gender")String gender);


    //当参数类型为Collection接口时，转换为Map，Map的key为collection(mybatis版本3.3之后)
//    Person getPersonByCollection(Collection list);

    //当参数类型为数组时，转换为Map，Map的key为array
//    Person getPersonByCollection(int[] ids);

    //使用注解来完成集合等类型传递
    Person getPersonByCollection(@Param("test")int[] ids);

    //返回结果是set/list/array等，需要使用foreach元素进行遍历输出
    List<Person> getListPersonByIds(int[] ids);
}
