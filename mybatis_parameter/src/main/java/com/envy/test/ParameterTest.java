package com.envy.test;

import com.envy.bean.Person;
import com.envy.dao.PersonMapper;
import com.mysql.jdbc.interceptors.SessionAssociationInterceptor;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;


import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParameterTest {

    public static SqlSessionFactory sqlSessionFactory=null;

    public static SqlSessionFactory getSqlSessionFactory(){
        if(sqlSessionFactory==null){
            String resource = "mybatis-config.xml";
            try {
                InputStream is = Resources.getResourceAsStream(resource);
                sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sqlSessionFactory;
    }


    //测试单个参数
    @Test
    public void deletePerson(){
        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
        SqlSession sqlSession = getSqlSessionFactory().openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        personMapper.deletePerson(6);
        //mybatis的事务不会自动提交，因此需要借助于SqlSession的commit方法去提交事务
        sqlSession.commit();
    }


    //测试多个参数

    //第一种封装成POJO对象
//    @Test
//    public void selectByUserNameAndGender(){
//        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
//        SqlSession sqlSession = getSqlSessionFactory().openSession();
//        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
//        Person person = personMapper.selectByUserNameAndGender(new Person("jack","男"));
//        System.out.println(person);
//        sqlSession.commit();
//    }

    //第二种封装成Map对象
//    @Test
//    public void selectByUserNameAndGender(){
//        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
//        SqlSession sqlSession = getSqlSessionFactory().openSession();
//        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
//        Map<String,Object> param = new HashMap<String, Object>();
//        param.put("name","bob");
//        param.put("gender","男");
//        Person person = personMapper.selectByUserNameAndGender(param);
//        System.out.println(person);
//    }


    //第三种使用@Param注解
    @Test
    public void selectByUserNameAndGender(){
        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
        SqlSession sqlSession = getSqlSessionFactory().openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        Person person = personMapper.selectByUserNameAndGender("marry","女");
        System.out.println(person);
    }


    //当参数类型为Collection接口时，转换为Map，Map的key为collection(mybatis版本3.3之后)；
//    @Test
////    public void getPersonByCollection(){
////        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
////        SqlSession sqlSession = getSqlSessionFactory().openSession();
////        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
////        Person person = personMapper.getPersonByCollection(Arrays.asList(1,2,3));
////        System.out.println(person);
////    }


//    当参数类型为数组时，转换为Map，Map的key为array
//    @Test
//    public void getPersonByCollection(){
//        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
//        SqlSession sqlSession = getSqlSessionFactory().openSession();
//        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
//        Person person = personMapper.getPersonByCollection(new int[]{1,2,3});
//        System.out.println(person);
//    }


    //foreach遍历
    @Test
    public void getPersonByCollection(){
        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
        SqlSession sqlSession = getSqlSessionFactory().openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        List<Person> personList = personMapper.getListPersonByIds(new int[]{1,2,3,4,5,6});
        System.out.println(personList);
    }
}


