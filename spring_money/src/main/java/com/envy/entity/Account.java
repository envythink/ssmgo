package com.envy.entity;

public class Account {
    private int id;
    private String name;
    private Long money;

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public Long getMoney(){
        return money;
    }

    public void setMoney(Long money){
        this.money = money;
    }

    public String toString(){
        return "Account:id is"+id+";name is"+ name+ ";money is"+money;
    }
}
