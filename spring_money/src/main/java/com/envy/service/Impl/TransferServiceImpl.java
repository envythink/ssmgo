package com.envy.service.Impl;

import com.envy.dao.TransferDao;
import com.envy.service.TransferService;
import org.springframework.transaction.annotation.Transactional;

public class TransferServiceImpl implements TransferService {

    private TransferDao transferDao;

    public void setTransferDao(TransferDao transferDao){
        this.transferDao=transferDao;
    }

    //转账操作，source支出方账户，destination收款方账户，amount转账金额
    @Transactional
    public void transferMoney(String source, String destination, Long amount) {
        //付款操作
        transferDao.payMoney(source,amount);
//        int i = 100/0;//此处用于测试抛异常时是否会回滚
        //收款操作
        transferDao.collectMoney(destination,amount);
    }
}
