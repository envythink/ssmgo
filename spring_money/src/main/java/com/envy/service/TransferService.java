package com.envy.service;

public interface TransferService {
    void transferMoney(String source,String destination, Long amount);
}
