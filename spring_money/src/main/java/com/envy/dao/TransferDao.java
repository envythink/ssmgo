package com.envy.dao;

public interface TransferDao {
    //付款,name账户名称，amount支出金额
    void payMoney(String name,Long amount);

    //收款，name账户名称，amount收到金额
    void collectMoney(String name,Long amount);

}
