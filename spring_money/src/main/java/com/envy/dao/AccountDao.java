package com.envy.dao;
import com.envy.entity.Account;


public interface AccountDao {
    void save(Account account);
}
