package com.envy.dao.Impl;

import com.envy.dao.TransferDao;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

public class TransferDaoImpl extends JdbcDaoSupport implements TransferDao {
    //付款,name账户名称，amount支出金额
    public void payMoney(String name, Long amount) {
        String sql = "update account set money = money-? where name=?";
        this.getJdbcTemplate().update(sql,amount,name);
    }

    //收款，name账户名称，amount收到金额
    public void collectMoney(String name, Long amount) {
        String sql = "update account set money = money+? where name=?";
        this.getJdbcTemplate().update(sql,amount,name);
    }
}
