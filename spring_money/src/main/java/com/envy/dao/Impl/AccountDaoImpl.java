package com.envy.dao.Impl;

import com.envy.dao.AccountDao;
import com.envy.entity.Account;
import org.springframework.jdbc.core.support.JdbcDaoSupport;


//dao层，继承JdbcDaoSupport
public class AccountDaoImpl extends JdbcDaoSupport implements AccountDao {

    public void save(Account account) {
        String sql = "insert into account(name,money)values(?,?)";
        getJdbcTemplate().update(sql,account.getName(),account.getMoney());  // 从父类JdbcDaoSupport中获取jdbcTemplate
    }
}
