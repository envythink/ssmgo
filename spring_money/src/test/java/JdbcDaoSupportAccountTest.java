import com.envy.dao.AccountDao;
import com.envy.entity.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class JdbcDaoSupportAccountTest {

    @Autowired
    private AccountDao accountDao;

    @Test
    public void testOne(){
        Account account = new Account();
        account.setName("小红");
        account.setMoney(888L);
        accountDao.save(account);
    }
}
