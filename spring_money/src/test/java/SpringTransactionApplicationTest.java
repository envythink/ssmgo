import com.envy.service.TransferService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class SpringTransactionApplicationTest {

    @Autowired
    private TransferService transferService;

    @Resource(name="transferServiceProxy")
    private TransferService transferServiceProxy;

    @Test
    public void TestOne(){
        //注意，此处使用的是代理对象transferServiceProxy，而不是transferService
        transferServiceProxy.transferMoney("小明","小白",66L);
    }
}
