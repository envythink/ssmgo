package com.envy.test;

import com.envy.bean.Person;
import com.envy.dao.PersonMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mysql.jdbc.interceptors.SessionAssociationInterceptor;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;


import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ParameterTest {

    public static SqlSessionFactory sqlSessionFactory = null;

    public static SqlSessionFactory getSqlSessionFactory() {
        if (sqlSessionFactory == null) {
            String resource = "mybatis-config.xml";
            try {
                InputStream is = Resources.getResourceAsStream(resource);
                sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sqlSessionFactory;
    }


    //使用foreach标签同时借助于Mysql数据库连接属性实现数据批量插入
    @Test
    public void insertDataByForeachAndMysql() {
        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接
        SqlSession sqlSession = getSqlSessionFactory().openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        List<Person> personList = new ArrayList<Person>();
        for (int i = 0; i < 5; i++) {
            Person person = new Person();
            person.setUsername("good" + i);
            person.setEmail("day" + i);
            person.setGender("女");
            personList.add(person);
        }
        personMapper.addPersonsByBatch(personList);
        sqlSession.commit();
    }


    //    基于SqlSession的ExecutorType实现数据批量插入
    @Test
    public void insertDataByExecutorType() {
        //SqlSessionFactory相当于数据库，SqlSession相当于一次连接(此处启用了Batch批处理模式)
        //ExecutorType类型有BATCH/SIMPLE/REUSE三种方式
        SqlSession sqlSession = getSqlSessionFactory().openSession(ExecutorType.BATCH);
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        for (int i =0;i<10000;i++){
            personMapper.addPerson(new Person("envy","envy@qq.com","男"));
        }
        sqlSession.commit();
        sqlSession.close();

    }

    //测试Interceptor接口
//    @Test
//    public void selectPersonById(){
//        SqlSession sqlSession = this.getSqlSessionFactory().openSession();
//        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
//        Person person = personMapper.getPersonById(1001);
//        System.out.println(person);
//        sqlSession.commit();
//        sqlSession.close();
//    }

    //测试PageHelper分页插件
    @Test
    public void testPageHelper(){
        SqlSession sqlSession = this.getSqlSessionFactory().openSession();
        PersonMapper personMapper = sqlSession.getMapper(PersonMapper.class);
        List<Person> personList = personMapper.getAllPerson();
        Page<Object> page = PageHelper.startPage(1,10) ;
        for(Person person:personList){
            System.out.println(person.getId());
        }
        page.getPageSize(); //page size
        page.getPageNum(); //current number
        sqlSession.commit();
        sqlSession.close();
    }

}


