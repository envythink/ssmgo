package com.envy.dao;

import com.envy.bean.Department;

public interface DepartmentMapper {
    Department searchById(Integer id);
}
