package com.envy.dao;

import com.envy.bean.Person;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.plugin.Intercepts;
import org.omg.CORBA.INTERNAL;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface PersonMapper {
    void deletePerson(Integer id);

    //使用foreach标签实现数据批量插入
    int addPersons(@Param("persons") List<Person> personList);

    //使用foreach标签同时借助于Mysql数据库连接属性实现数据批量插入
    int addPersonsByBatch(@Param("personBatch") List<Person> personList);


    //基于SqlSession的ExecutorType实现数据批量插入
    int addPerson(Person person);

    //测试Interceptor接口
    Person getPersonById(Integer id);

    //测试PageHelper分页插件
    List<Person> getAllPerson();
}
