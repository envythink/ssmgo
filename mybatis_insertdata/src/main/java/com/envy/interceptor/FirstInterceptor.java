package com.envy.interceptor;

import org.apache.ibatis.executor.resultset.ResultSetHandler;
import org.apache.ibatis.plugin.*;


import java.sql.Statement;
import java.util.Properties;


//插件签名，告诉mybatis当前插件用来拦截哪个对象的哪个方法
@Intercepts({@Signature(type = ResultSetHandler.class,method ="handleResultSets",args = Statement.class)})
public class FirstInterceptor implements Interceptor {

    //拦截目标对象的目标方法
    public Object intercept(Invocation invocation) throws Throwable {
        System.out.println("拦截的目标对象："+invocation.getTarget());
        System.out.println("拦截的目标对象的方法是："+invocation.getMethod());
        System.out.println("拦截的目标对象的参数是"+invocation.getArgs());
        Object object = invocation.proceed(); //执行目标对象中的目标方法
        return object;
    }

    //包装目标对象，用于给目标对象创建代理对象
    public Object plugin(Object o) {
        System.out.println("将要包装的目标对象为："+o);
        return Plugin.wrap(o,this); //返回被代理的对象
    }


    //用于获取mybatis-config.xml中配置的plugin参数
    public void setProperties(Properties properties) {
        System.out.println("插件配置的初始化参数为："+properties);
    }
}
