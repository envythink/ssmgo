package com.envy.bean;

public class Person {
    private Integer id;
    private String username;
    private String email;
    private String gender;
    private Integer departmentId;
    private Department department;

    public Person(){}

    public Person(String username, String gender) {
        this.username = username;
        this.gender = gender;
    }

    public Person(String username, String email, String gender) {
        this.username = username;
        this.email = email;
        this.gender = gender;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", departmentId=" + departmentId +
                ", department=" + department +
                '}';
    }
}
