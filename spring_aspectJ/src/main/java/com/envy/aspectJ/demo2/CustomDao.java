package com.envy.aspectJ.demo2;

public interface CustomDao {
    public void save();
    public void delete();
    public void update();
    public void findOne();
    public void findAll();
}
