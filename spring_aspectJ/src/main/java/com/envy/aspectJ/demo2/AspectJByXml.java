package com.envy.aspectJ.demo2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * XML配置方式
 * **/
public class AspectJByXml {
    //前置通知
    public void beforeXMl(JoinPoint joinPoint){
        System.out.println("xml方式的前置通知"+joinPoint);
    };

    //后置通知
    public String afterReturningXMl(Object object){
        System.out.println("xml方式的后置通知");
        return "********后置通知******"+object;
    };

    //环绕通知
    public Object aroundXML(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        System.out.println("xml方式的环绕前通知");
        Object obj=proceedingJoinPoint.proceed();
        System.out.println("xml方式的环绕后通知");
        return obj;
    }

    //异常抛出通知
    public void afterThrowingXML(Throwable e){
        System.out.println("****xml方式的异常抛出****"+e);
    }

    //最终通知
    public void afterXML(){
        System.out.println("xml方式的最后通知");
    }

}
