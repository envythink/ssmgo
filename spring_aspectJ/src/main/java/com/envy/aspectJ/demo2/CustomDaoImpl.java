package com.envy.aspectJ.demo2;

public class CustomDaoImpl implements CustomDao {
    @Override
    public void save() {
        System.out.println("**保存客户信息**");
    }

    @Override
    public void delete() {
        System.out.println("**删除客户信息**");
    }

    @Override
    public void update() {
        System.out.println("**修改客户信息**");
    }

    @Override
    public void findOne() {
        System.out.println("**查询某位客户信息**");
//        int i= 1/0;
    }

    @Override
    public void findAll() {
        System.out.println("**查询所有客户信息**");
    }
}
