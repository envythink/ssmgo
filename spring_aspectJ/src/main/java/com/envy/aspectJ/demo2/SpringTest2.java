package com.envy.aspectJ.demo2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext2.xml")
public class SpringTest2 {
    @Resource(name = "customDaoImpl")
    private CustomDao customDao;

    @Test
    public void testTwo(){
        customDao.save();
        customDao.delete();
        customDao.update();
        customDao.findOne();
        customDao.findAll();
    };
}
