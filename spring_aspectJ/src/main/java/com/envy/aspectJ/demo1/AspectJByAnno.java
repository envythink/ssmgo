package com.envy.aspectJ.demo1;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * 切面类
 **/
@Aspect
public class AspectJByAnno {

    //前置通知类型
//    @Before(value="execution(* com.envy.aspectJ.demo1.ProductDao.*(..))")
//    @Before(value="execution(* com.envy.aspectJ.demo1.ProductDao.save(..))")
//    public void before(JoinPoint joinPoint){
//        System.out.println("前置通知"+joinPoint);
//    }

    @Before(value="mySave()")
    public void before(JoinPoint joinPoint){
        System.out.println("前置通知"+joinPoint);
    }

    //后置通知类型
//    @AfterReturning(value="execution(* com.envy.aspectJ.demo1.ProductDao.update(..))")
//    public void afterReturning(){
//        System.out.println("后置通知");
//    }

    //后置通知类型
    @AfterReturning(value="execution(* com.envy.aspectJ.demo1.ProductDao.update(..))", returning = "returning")
    public void afterReturning(Object returning){
        System.out.println("后置通知"+returning);
    }

    //环绕通知类型
    @Around(value="execution(* com.envy.aspectJ.demo1.ProductDao.delete(..))")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        System.out.println("****环绕前通知****");
        Object object = proceedingJoinPoint.proceed();
        System.out.println("****环绕后通知****");
        return object;
    }

    //异常抛出通知类型
    @AfterThrowing(value="execution(* com.envy.aspectJ.demo1.ProductDao.findOne(..))",throwing = "e")
    public void afterThrowing(Throwable e){
        System.out.println("****异常抛出****"+e);
    }

    //最终通知类型
    @After(value="execution(* com.envy.aspectJ.demo1.ProductDao.findAll(..))")
    public void after(){
        System.out.println("****最终通知****");
    }


    @Pointcut(value = "execution(* com.envy.aspectJ.demo1.ProductDao.save(..))")
    private void mySave(){};
}
