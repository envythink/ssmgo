package com.envy.controller;

import com.envy.entity.User;
import com.sun.org.glassfish.gmbal.ParameterNames;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public String login(){
        return "login";
    }


    @RequestMapping(value = "/logined")
    public String logined(@RequestParam("name")String name, @RequestParam("password")String password, HttpSession session){
        if("envy".equals(name)&&"1234".equals(password)){
            User user = new User();
            user.setName(name);
            user.setPassword(password);
            session.setAttribute("USER",user);
            return "redirect:user/search";
        }else{
            return "redirect:login";
        }
    }
}
