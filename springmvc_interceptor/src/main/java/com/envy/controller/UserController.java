package com.envy.controller;

import com.envy.entity.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/user")
public class UserController {

    //搜索
    @RequestMapping(value = "/search")
    public String search(){
        System.out.println("2、interceptor*****search");
        return "search";
    }

    //修改密码
    @RequestMapping(value = "/updatePwd")
    public String updatePwd(){
        System.out.println("interceptor*****updatePwd");
        return "updatePwd";
    }

    //修改签名
    @RequestMapping(value = "/updateSig")
    public String updateSig(){
        System.out.println("interceptor*****updateSig");
        return "updateSig";
    }
}
