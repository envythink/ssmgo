package com.envy.global;


import com.envy.entity.Log;
import com.envy.entity.Staff;
import com.envy.service.LogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect
public class LogAdvice {

    @Autowired
    private LogService logService;

    //操作日志
    //任意前面修饰符com.envy.controller包下所有的类的所有方法任意参数进行织入，但是登录相关会以另一个日志进行记录，因此需要过虑掉
    //还有就是有些方法是toAdd,toEdit等，这些也是不需要织入的，因为toAdd和add都记录的话会造成重复记录
    @After("execution(* com.envy.controller.*.* (..)) && !execution (* com.envy.controller.SelfController.*(..)) && !execution (* com.envy.controller.*.to*(..))")
    public void operationLog(JoinPoint joinPoint){
        //增强
        Log log = new Log();
        //获取moudle信息，如staff,department,log等
        log.setMoudle(joinPoint.getTarget().getClass().getSimpleName());
        //获取operation信息，其实就是service中的各个方法名称
        log.setOperation(joinPoint.getSignature().getName());
        //获取operationPerson信息，其实这个可以从request中的session获取,而request则是joinPoint的第一个参数
        HttpServletRequest request =(HttpServletRequest) joinPoint.getArgs()[0];
        Staff staff = (Staff) request.getSession().getAttribute("USER");
        log.setOperationPerson(staff.getAccount());
        log.setResult("成功");
        logService.addOperationLog(log);
    }


    //系统日志,只有当系统出现异常才会记录，之前的toAdd、toEdit方法也有可能会出异常，因此需要进行织入
    @AfterThrowing(throwing ="e", pointcut = "execution(* com.envy.controller.*.* (..)) && !execution (* com.envy.controller.SelfController.*(..))")
    public void systemLog(JoinPoint joinPoint,Throwable e){
        //增强
        Log log = new Log();
        //获取moudle信息，如staff,department,log等
        log.setMoudle(joinPoint.getTarget().getClass().getSimpleName());
        //获取operation信息，其实就是service中的各个方法名称
        log.setOperation(joinPoint.getSignature().getName());
        //获取operationPerson信息，其实这个可以从request中的session获取,而request则是joinPoint的第一个参数
        HttpServletRequest request =(HttpServletRequest) joinPoint.getArgs()[0];
        Staff staff = (Staff) request.getSession().getAttribute("USER");
        log.setOperationPerson(staff.getAccount());
        log.setResult(e.getClass().getSimpleName());
        logService.addSystemLog(log);
    }


    //登录日志
    @After("execution (* com.envy.controller.SelfController.login(..))")
    public void loginLog(JoinPoint joinPoint){
        log(joinPoint);
    }

    //登出日志
    @Before("execution (* com.envy.controller.SelfController.logout(..))")
    public void logoutLog(JoinPoint joinPoint){
        log(joinPoint);
    }


    private void log(JoinPoint joinPoint){
        Log log = new Log();
        //获取moudle信息，如staff,department,log等
        log.setMoudle(joinPoint.getTarget().getClass().getSimpleName());
        //获取operation信息，其实就是service中的各个方法名称
        log.setOperation(joinPoint.getSignature().getName());
        //获取operationPerson信息，其实这个可以从request中的session获取,而request则是joinPoint的第一个参数
        HttpServletRequest request =(HttpServletRequest) joinPoint.getArgs()[0];
        Staff staff = (Staff) request.getSession().getAttribute("USER");
        if(staff ==null){
            log.setOperationPerson(request.getParameter("account"));
            log.setResult("失败");
        }else{
            log.setOperationPerson(staff.getAccount());
            log.setResult("成功");
        }
        logService.addLoginLog(log);
    }

}
