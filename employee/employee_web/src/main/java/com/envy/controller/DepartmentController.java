package com.envy.controller;

import com.envy.dao.DepartmentDao;
import com.envy.entity.Department;
import com.envy.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller("departmentController")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    //department/list.do
    //部门所有信息展示
    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Department> list = departmentService.getAll();
        request.setAttribute("LIST",list);
        System.out.println(list);
        //请求转发，注意相对路径（因为控制器有两层/department/list.do，而jsp是在第一层）
        request.getRequestDispatcher("../department_list.jsp").forward(request,response);
    }

    //department/toAdd.do
    public void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("../department_add.jsp").forward(request,response);
    }

    //department/add.do
    public void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String address = request.getParameter("address");

        Department department = new Department();
        department.setName(name);
        department.setAddress(address);
        departmentService.add(department);
        //页面重定向，url会发生变化，注意是跳转到这个控制器list.do而不是页面
        response.sendRedirect("list.do");
    }

    //department/toEdit.do
    public void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //注意编辑页面需要携带修改对象的信息
        Integer id = Integer.parseInt(request.getParameter("id"));
        Department department = departmentService.get(id);
        request.setAttribute("OBJ",department);
        request.getRequestDispatcher("../department_edit.jsp").forward(request,response);
    }

    //department/edit.do
    public void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        String address = request.getParameter("address");

        Department department = departmentService.get(id);
        department.setName(name);
        department.setAddress(address);
        departmentService.edit(department);
        //页面重定向，url会发生变化，注意是跳转到这个控制器list.do而不是页面
        response.sendRedirect("list.do");
    }

    //department/remove.do
    public void remove(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //注意删除页面需要携带删除对象的信息
        Integer id = Integer.parseInt(request.getParameter("id"));
        departmentService.remove(id);
        response.sendRedirect("list.do");
    }

}
