package com.envy.controller;

import com.envy.entity.Staff;
import com.envy.service.SelfService;
import com.envy.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller("selfController")
public class SelfController {

    @Autowired
    private SelfService selfService;

    //toLogin.do
    public void toLogin(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("login.jsp").forward(request,response);
    }

    //login.do
    public void login(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        String account = request.getParameter("account");
        String password = request.getParameter("password");
        Staff staff = selfService.login(account,password);
        if(staff==null){
          response.sendRedirect("toLogin.do");
        }else{
            request.getSession().setAttribute("USER",staff);
            response.sendRedirect("index.do");
        }
    }

    //logout.do
    public void logout(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("USER",null);
        response.sendRedirect("toLogin.do");
    }

    //index.do
    public void index(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("index.jsp").forward(request,response);
    }

    //查看个人信息
    //访问路径/self/info.do
    public void info(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("../info.jsp").forward(request,response);
    }

    //访问路径/self/toChangePassword.do
    public void toChangePassword(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("../change_password.jsp").forward(request,response);
    }

    //访问路径/self/changePassword.do
    public void changePassword(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        String password = request.getParameter("password");
        String password1 = request.getParameter("password1");
        Staff staff = (Staff)request.getSession().getAttribute("USER");
        if(!staff.getPassword().equals(password)){
            response.sendRedirect("toChangePassword.do");
        }else{
            selfService.changePassword(staff.getId(),password1);
//            response.sendRedirect("../logout.do");
            //注意此代码是保证当用户退出后，页面会重定向到frame外面，而不是frame内部，这一点很重要。
            response.getWriter().print("<script type=\"text/javascript\">parent.location.href=\"../logout.do\"</script>");
        }
    }
}
