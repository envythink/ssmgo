package com.envy.controller;

import com.envy.dao.StaffDao;
import com.envy.entity.Department;
import com.envy.entity.Staff;
import com.envy.service.DepartmentService;
import com.envy.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

@Controller("staffController")
public class StaffController {

    @Autowired
    private StaffService staffService;

    @Autowired
    private DepartmentService departmentService;

    //staff/list.do
    public void list(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Staff> list = staffService.getAll();
        request.setAttribute("LIST", list);
        request.getRequestDispatcher("../staff_list.jsp").forward(request, response);
    }

    //staff/toAdd.do
    public void toAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //注意由于这里的员工信息涉及到部门信息，因此需要从DepartmentService中获取
        List<Department> list = departmentService.getAll();
        request.setAttribute("DLIST",list);
        request.getRequestDispatcher("../staff_add.jsp").forward(request, response);
    }

    //staff/add.do
    public void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String account = request.getParameter("account");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        String idNumber = request.getParameter("idNumber");
        String note = request.getParameter("note");
        Date birthday = null;

        try {
            birthday = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("birthday"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));

        Staff staff = new Staff();
        staff.setAccount(account);
        staff.setName(name);
        staff.setSex(sex);
        staff.setIdNumber(idNumber);
        staff.setNote(note);
        staff.setBirthday(birthday);
        staff.setDepartmentId(departmentId);
        staffService.add(staff);
        response.sendRedirect("list.do");
    }


    //staff/toEdit.do
    public void toEdit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //注意编辑页面需要携带修改对象的信息,也包括部门信息
        Integer id = Integer.parseInt(request.getParameter("id"));
        Staff staff = staffService.get(id);
        request.setAttribute("OBJ", staff);
        List<Department> list = departmentService.getAll();
        request.setAttribute("DLIST",list);
        request.getRequestDispatcher("../staff_edit.jsp").forward(request, response);
    }

    //staff/edit.do
    public void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        String account = request.getParameter("account");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        String idNumber = request.getParameter("idNumber");
        String note = request.getParameter("note");
        Date birthday = null;

        try {
            birthday = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("birthday"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Integer departmentId = Integer.parseInt(request.getParameter("departmentId"));

        Staff staff = staffService.get(id);
        staff.setName(name);
        staff.setSex(sex);
        staff.setIdNumber(idNumber);
        staff.setNote(note);
        staff.setBirthday(birthday);
        staff.setDepartmentId(departmentId);
        staff.setAccount(account);
        staffService.edit(staff);
        //页面重定向，url会发生变化，注意是跳转到这个控制器list.do而不是页面
        response.sendRedirect("list.do");
    }

    //staff/remove.do
    public void remove(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer id = Integer.parseInt(request.getParameter("id"));
        staffService.remove(id);
        //页面重定向，url会发生变化，注意是跳转到这个控制器list.do而不是页面
        response.sendRedirect("list.do");
    }

    //staff/detail.do
    public void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //注意编辑页面需要携带修改对象的信息
        Integer id = Integer.parseInt(request.getParameter("id"));
        Staff staff = staffService.get(id);
        request.setAttribute("OBJ", staff);
        request.getRequestDispatcher("../staff_detail.jsp").forward(request, response);
    }

}
