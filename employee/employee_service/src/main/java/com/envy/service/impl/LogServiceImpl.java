package com.envy.service.impl;

import com.envy.dao.LogDao;
import com.envy.entity.Log;
import com.envy.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("logService")
public class LogServiceImpl implements LogService {

    @Autowired
    private LogDao logDao;

    public void addSystemLog(Log log) {
        log.setOperationTime(new Date());
        log.setOperationType("system");
        logDao.insert(log);
    }

    public void addLoginLog(Log log) {
        log.setOperationTime(new Date());
        log.setOperationType("login");
        logDao.insert(log);
    }

    public void addOperationLog(Log log) {
        log.setOperationTime(new Date());
        log.setOperationType("operation");
        logDao.insert(log);
    }

    public List<Log> getSystemLog() {
        return logDao.selectByType("system");
    }

    public List<Log> getLoginLog() {
        return logDao.selectByType("login");
    }

    public List<Log> getOperationLog() {
        return logDao.selectByType("operation");
    }
}
