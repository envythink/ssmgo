package com.envy.service;

import com.envy.entity.Log;

import java.util.List;

public interface LogService {
    //添加日志
    void addSystemLog(Log log);
    void addLoginLog(Log log);
    void addOperationLog(Log log);

    //查看日志
    List<Log> getSystemLog();
    List<Log> getLoginLog();
    List<Log> getOperationLog();

}
