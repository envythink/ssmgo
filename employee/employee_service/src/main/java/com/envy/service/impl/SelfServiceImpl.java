package com.envy.service.impl;

import com.envy.dao.SelfDao;
import com.envy.dao.StaffDao;
import com.envy.entity.Staff;
import com.envy.service.SelfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("selfService")
public class SelfServiceImpl implements SelfService {

    @Autowired
    private SelfDao selfDao;

    @Autowired
    private StaffDao staffDao;

    //登录
    public Staff login(String account, String password) {
        Staff staff = selfDao.selectByAccount(account);
        if(staff==null)return null;
        if(staff.getPassword().equals(password))return staff;
        return null;
    }

    //修改密码
    public void changePassword(Integer id, String password) {
        Staff staff = staffDao.searchById(id);
        staff.setPassword(password);
        staffDao.update(staff);
    }
}
