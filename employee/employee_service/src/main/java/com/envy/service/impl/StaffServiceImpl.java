package com.envy.service.impl;

import com.envy.dao.StaffDao;
import com.envy.entity.Staff;
import com.envy.service.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("staffService")
public class StaffServiceImpl implements StaffService {

    @Autowired
    private StaffDao staffDao;

    public void add(Staff staff) {
        //一些员工的初始信息，必须在Service层进行处理
        staff.setPassword("1234");
        staff.setWorkTime(new Date());
        staff.setStatus("正常");
        staffDao.insert(staff);
    }

    public void remove(Integer id) {
        staffDao.delete(id);
    }

    public void edit(Staff staff) {
        staffDao.update(staff);
    }

    public Staff get(Integer id) {
        return staffDao.searchById(id);
    }

    public List<Staff> getAll() {
        return staffDao.searchAll();
    }
}
