package com.envy.dao;

import com.envy.entity.Staff;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("staffDao")
public interface StaffDao {
    void insert(Staff staff);
    void delete(Integer id);
    void update(Staff staff);
    Staff searchById(Integer id);
    List<Staff> searchAll();
}
