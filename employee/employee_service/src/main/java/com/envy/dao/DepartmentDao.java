package com.envy.dao;

import com.envy.entity.Department;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("departmentDao")
public interface DepartmentDao {
    void insert(Department department);
    void delete(Integer id);
    void update(Department department);
    Department searchById(Integer id);
    List<Department> searchAll();
}
