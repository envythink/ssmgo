drop database if exists employee;
create database employee;
use employee;
create table department(
id int primary key auto_increment,
name varchar(20) not null,
address varchar(100)
);
create table staff(
id int primary key auto_increment,
account varchar(20) not null,
password varchar(20) not null,
status varchar(20) not null,
department_id int,
name varchar(20),
sex varchar(20),
id_number varchar(20),
work_time datetime,
leave_time datetime,
birthday date,
note varchar(200)
);

create table log(
id int primary key auto_increment,
operation_time datetime not null,
operation_type varchar(10) not null,
operation_person varchar(20) not null,
moudle varchar(20) not null,
operation varchar(20) not null,
result varchar(20) not null
);

alter table staff add constraint fk_staff_department foreign key(department_id) references department(id);